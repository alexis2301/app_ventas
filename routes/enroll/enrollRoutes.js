const express = require('express')
const router = express.Router()
const usuario = require('../../controllers/enrollPersona')
const auth = require('../../middleware/authMiddleware')

// router.get('/addUsuario',auth.authenticate,usuario.enrollUsuario)
router.post('/addUsuario',usuario.enroll)

module.exports = router
