const express = require('express')
const router = express.Router()
const enrollRoutes = require('./enroll/enrollRoutes')


// router.post('/enroll', auth.authenticate ,enrollRoutes) 
router.use('/enroll', enrollRoutes) 

module.exports = router
