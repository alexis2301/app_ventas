const myConnection = require('../database/config')
const utils = require('../resources/utils')
const jwt = require('jsonwebtoken')
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
        host: "mail.AutomatizaCorp.cl",
        port: 465,
        secure: true,
        auth: {
          user: 'mailer.noreply@AutomatizaCorp.cl',
          pass: '@]]Q82fVbbJ#@]]Q82fVbbJ#'
        },
        tls: {
            ciphers:'SSLv3'
        }
    });

// const doLogin =  async (request,response)=>{
//      const sql = `select id,username from users where username = ? and password = ?`  
//      console.log(sql)
//      const res =  myConnection.query(sql,
//         [
//             request.body.username,
//             request.body.password
//         ],
//         function(err, results) {
//             console.log(results)
//             console.log(err)
//             if(results[0]){
//                 // response.json({message:"Login Exitoso", flag:true})
//                 user = results[0]
//                 Object.assign(user, {exp: Math.floor(Date.now() / 1000) + (60*15)})
//                 token = jwt.sign(
//                     user,
//                     process.env.ACCESS_TOKEN_SECRET,
//                     )
//                 respuesta = {
//                     message:"Login Exitoso",
//                     userData : JSON.stringify(results[0]),
//                     token
//                 }
//                 response.render('app/dashboard',{locals:respuesta})
//             }else{
//                 respuesta = {
//                     message:"Login Fallido",
//                     userData : '',
//                     token : ''
//                 }
//                 response.render('app/error',{locals:respuesta})
//             }
//         }
//     );
// }
const enroll = async (request,response) => {
    console.log(request.body)
    const name = request.body.name
    const lastname = request.body.lastname
    const address = request.body.address
    const email = request.body.email
    const password = request.body.password
    const verified = request.body.verified
    const gs_data = request.body.gs_data
    const sql = `insert into users(name,lastname,address,email,password,verified,gs_data,created_at) values(?,?,?,?,?,0,'',now())`  
    const res =  myConnection.query(sql,
    [
        name,
        lastname,
        address,
        email,
        password,
        verified,
        gs_data,
    ],
    async function(err, results) {
        if(err){
            console.log(err)
            response.json({status:false,message:"Error"})
        }else{
            token = jwt.sign({name,email,password},process.env.ACCESS_TOKEN_SECRET)
            urlConfirm = 'http://localhost:3000/login/confirm?token='+token
            const info = await transporter.sendMail({
                from: '"AutomatizaCorp 👻" <noreply@AutomatizaCorp.cl>', // sender address
                to: `${email}, ${email}`, // list of receivers
                subject: "Confirmacion Email ✔", // Subject line
                text: "Confirmacion Email", // plain text body
                html: `<b>Confirme Email en la siguiente direccion -> ${urlConfirm}</b>`,
              });
            response.json({status:true,message:"Debe verificar su direccion de email"})
        }
    }
    );
}
const verify = async(id) => {
    sql = 'update users set verified = 1 where id = ?'
    const res =  myConnection.query(sql,
        [
            id
        ],
        async function(err, results) {
            if(err){
                return false
            }else{
                return true    
            }
        }
        );
}
const confirm = async (request,response) =>{
    token = request.query.token
    data = jwt.decode(token,process.env.ACCESS_TOKEN_SECRET)
    console.log(data)
    sql = 'select * from users where name = ? and email = ? and password = ? and verified = 0'
    const res = myConnection.query(sql,
        [
            data.name,
            data.email,
            data.password
        ],
        function(err, results) {
            console.log("resultados = "+results)
            if(err){
                response.json({status:false,message:"Error"})
            }else{
                if(results){
                    if (verify(results[0].id)){
                        response.json({status:true,message:"Usuario verificado"})
                    }else{
                        response.json({status:false,message:"Error al verificar"})
                    }
                }else{
                    response.json({status:false,message:"Usuario no existe"})
                }
            }
        }
        );
}

module.exports = {
    // doLogin,
    enroll,
    confirm
}